#include "traversetree.h"
#include "compare_files.h"
#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <string.h>



void createDoubleDoubleList (doubleDoubleLinkedListNode* Head, char* Filename)
{
    
    doubleDoubleLinkedListNode* position;
    //initialize to first position in data structure
    position = Head->next;
    if (!Head->next)
    {   
	//If  there is no argument, (First time?) create a new first node
        doubleDoubleLinkedListNode *newFile = (doubleDoubleLinkedListNode*) malloc (sizeof(doubleDoubleLinkedListNode));
	newFile->Filename = strdup(Filename);
        newFile->count = 1;
	newFile->previous = NULL;
	newFile->next = NULL;
	newFile->NextInstanceOfFile = NULL;
	newFile->PreviousInstanceOfFile = NULL;
        Head->next = newFile;
	// EJF free(Filename);
        return;
    }
    //"Position" tracks pointer to our current node in the list. Iterate over the linked list
    while (position != NULL)
    {
        //Compare the file at the current position with the file argument
        if (compareFiles(position->Filename , Filename) == 1)
        {
            //Files are the same.
            //Increment the count of duplicates for this node
            
            //Now look at the next node in the list
            doubleDoubleLinkedListNode* temp;
            temp = position;
            //iterate over the pointers to other tracked duplicates and increment their counts of dups as well
            while (temp != NULL)
            {   
                temp->count = temp->count + 1;
		if (temp->NextInstanceOfFile == NULL)
		{
			doubleDoubleLinkedListNode *newFile = (doubleDoubleLinkedListNode*) malloc (sizeof(doubleDoubleLinkedListNode));
        		newFile->Filename = strdup(Filename);
        		newFile->count = temp->count;
        		newFile->previous = NULL;
        		newFile->next = NULL;
        		newFile->NextInstanceOfFile = NULL;
        		newFile->PreviousInstanceOfFile = temp;
			temp->NextInstanceOfFile = newFile;
        		return;
		}
		temp = temp->NextInstanceOfFile;
		
                
                //Eventually this should be null and break the loop
                
            }
		/*
            while (position->previous->count < position->count)
            {
                //Sorting. ???
                printf("before sorting");
                if (position->next != NULL)
                {
                  position->next->previous = position->previous->next;
                }
                if (position->previous != NULL)
                {
                  position->previous->next = position->next->previous;
                  if (position->previous->previous != NULL)
                  {
                    position->previous->previous->next = position;
                    temp = position->previous->previous;
                    position->previous->previous = position; 
                    position->previous = temp;
                  }
                }
                
            }
		
            if (position->previous == NULL)
            {
                Head = position;
            }
	    */
            //At this  point, we should have updated all the duplicates for this file with the right counts
            
        }
        
        else 
        {
            //files are different. Set pointer to next node in the list to check it
            
            //If this position doesn't have a next  pointer, and isn't a duplicate of the current file, make a new one
            if (position->next == NULL )
            {
                doubleDoubleLinkedListNode *newFile = (doubleDoubleLinkedListNode*) malloc (sizeof(doubleDoubleLinkedListNode));
        	newFile->count = 1;
        	newFile->previous = NULL;
        	newFile->next = NULL;
        	newFile->NextInstanceOfFile = NULL;
        	newFile->PreviousInstanceOfFile = NULL;
    	        position->next = newFile;
                newFile->previous = position;
		newFile->Filename = strdup(Filename);
		// EJF free(Filename);
                position->next = newFile;
		position = newFile;
                return;
            }
	    
        }
	position = position->next;
    }
    return;
}



