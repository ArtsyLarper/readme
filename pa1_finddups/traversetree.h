#include <stdio.h>
#include <stdlib.h>

typedef struct doubleDoubleLinkedListNode doubleDoubleLinkedListNode;

struct doubleDoubleLinkedListNode { //does def belong here?
    doubleDoubleLinkedListNode* previous;
    doubleDoubleLinkedListNode* next;
    doubleDoubleLinkedListNode* NextInstanceOfFile;
    doubleDoubleLinkedListNode* PreviousInstanceOfFile;
    int count;
    char* Filename;
};

void createDoubleDoubleList (doubleDoubleLinkedListNode* Head, char* Filename);
