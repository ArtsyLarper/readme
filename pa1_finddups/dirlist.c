#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <dirent.h>



int main(int argc,char *argv[]){

	int fd;
	int len;
	size_t size;
  DIR *dir;
  struct dirent *entry;

	dir = opendir(argv[1]);
  if (dir == NULL) {
    printf("Error opening directory");
		exit(1);
	}
  while ((entry = readdir(dir)) != NULL) {
		if (strcmp(entry->d_name,".")==0 || strcmp(entry->d_name,"..")==0)
			continue;

		printf("Entry: %d %s\n",entry->d_type,entry->d_name);
    switch (entry->d_type) {


    	case DT_DIR:
			  printf("Directory!\n");
				break;
    	case DT_REG:
			  printf("Regular File!\n");
        break;
			default:
				printf("Unhandled file\n");
		}
	}
	
	printf("I'm Done\n");

}
