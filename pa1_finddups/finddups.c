#include <stdio.h>
#include "compare_files.h"
#include <string.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <dirent.h>
#include "traversetree.h"


int is_regular_file(const char *path)
{
    struct stat path_stat;
    stat(path, &path_stat);
    return S_ISREG(path_stat.st_mode);
}


void traverseTree(doubleDoubleLinkedListNode* ddList, char* directory_name) //returns beginning of doubledoublelinkedlist
{
	
	DIR *dir;
	struct dirent *entry;
	//int PATH_MAX = 4096;
	if (is_regular_file(directory_name))
	{
		createDoubleDoubleList(ddList, directory_name);
	}
	else
	{
		dir = opendir(directory_name);
		if (dir == NULL) {
			printf("Error opening directory");
			exit(1);
		}	
		while ((entry = readdir(dir)) != NULL) {
			if (strcmp(entry->d_name,".")==0 || strcmp(entry->d_name,"..")==0)
				{continue;}
			char path[PATH_MAX];
	        	snprintf(path, PATH_MAX, "%s/%s", directory_name, entry->d_name);

    			switch (entry->d_type) {

				case DT_DIR:
					traverseTree(ddList, path);
					break;
    				case DT_REG:
					createDoubleDoubleList(ddList , path);
        				break;
					//createNode
				default:
					printf("Unhandled file\n");
			}
		}	
		closedir(dir);
	}
	return;
 }

/*
doubleDoubleLinkedListNode* traverseTree(doubleDoubleLinkedListNode* ddList, char* directory_name) {
    DIR *dir = opendir(directory_name);
    if (dir == NULL) {
        printf("Error opening directory %s\n", directory_name);
        exit(1);
    }

    struct dirent *entry;
    while ((entry = readdir(dir)) != NULL) {
        if (strcmp(entry->d_name, ".") == 0 || strcmp(entry->d_name, "..") == 0) {
            continue;
        }

        char path[PATH_MAX];
        snprintf(path, PATH_MAX, "%s/%s", directory_name, entry->d_name);
        printf("Entry: %d %s\n", entry->d_type, path);

        switch (entry->d_type) {
            case DT_DIR:
                ddList = traverseTree(ddList, path);
                break;
            case DT_REG: {
                FILE *file = fopen(path, "r");
                if (file == NULL) {
                    printf("Error opening file %s\n", path);
                    exit(1);
                }
                ddList = createDoubleDoubleList(ddList, file);
                fclose(file);
                break;
            }
            default:
                printf("Unhandled file %s\n", path);
                break;
        }
    }

    closedir(dir);
    return ddList;
}
*/

void printTree (doubleDoubleLinkedListNode* Head)
{
	if (Head == NULL)
	{
		printf ("Error Head is NULL\n");
		return;
	}
	doubleDoubleLinkedListNode* temp1, *temp2, *temp3;
	temp1 = Head;
	temp2 = Head;
	int count = 1;
	while (temp1 != NULL)
	{
		count = 1;
		if (temp1->count >1)
		{
			while (temp2 != NULL)
			{	
				printf ("%d %d %s \n", temp2->count,count, temp2->Filename);
				temp2 = temp2->NextInstanceOfFile;
				count++;
			}
		}
		temp1=temp1->next;
		temp2 = temp1;
	}
	temp1 = Head;
	temp2 = Head;
	while (temp1 != NULL)
        {
                temp1=temp1->next;                
                        while (temp2 != NULL)
                        {
                                temp3 = temp2;
                                temp2 = temp2->NextInstanceOfFile;
				free(temp3->Filename);
				free(temp3);
                                
                        }
                
                temp2 = temp1;
        }
	return;
}


int main(int argc,char *argv[]){
	
	doubleDoubleLinkedListNode *Head = malloc(sizeof(doubleDoubleLinkedListNode));
  Head->previous = NULL;
  Head->next = NULL;
  Head->NextInstanceOfFile = NULL;
  Head->PreviousInstanceOfFile = NULL;
  Head->count = 0;
  Head->Filename = NULL;
	//enter the directory4
	if  (argc <= 1)
	{
		traverseTree(Head, ".");
	}
	else
	{
		for (int i = 1; i<argc; i++)
		{
			traverseTree(Head, argv[i]);
		}
	}
	printTree(Head);

	//free(Head);
	
	//find first file
	//put first file into linked list
	//increment int of number of instances
	//while directory still has files inside it 
	//if file is directory enter directory
	//if regular file compare to all previous files in the linkedlist 
	//if regular file is new add element to end of linked list
	//if regular file has been seen before increment count in item of first linked list then add element to next linkedlist, 
	//         then compare increment count and update position of file in the list
	
}


/*
#include <stdio.h>
#include "compare_files.h"
#include <string.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <dirent.h>
#include "traversetree.h"

doubleDoubleLinkedListNode* traverseTree(doubleDoubleLinkedListNode* ddList, const char* directory_name) {
    DIR *dir = opendir(directory_name);
    if (dir == NULL) {
        printf("Error opening directory %s\n", directory_name);
        exit(1);
    }

    struct dirent *entry;
    while ((entry = readdir(dir)) != NULL) {
        if (strcmp(entry->d_name, ".") == 0 || strcmp(entry->d_name, "..") == 0) {
            continue;
        }

        char path[PATH_MAX];
        snprintf(path, PATH_MAX, "%s/%s", directory_name, entry->d_name);
        printf("Entry: %d %s\n", entry->d_type, path);

        switch (entry->d_type) {
            case DT_DIR:
                ddList = traverseTree(ddList, path);
                break;
            case DT_REG: {
                FILE *file = fopen(path, "r");
                if (file == NULL) {
                    printf("Error opening file %s\n", path);
                    exit(1);
                }
                ddList = createDoubleDoubleList(ddList, file);
                fclose(file);
                break;
            }
            default:
                printf("Unhandled file %s\n", path);
                break;
        }
    }

    closedir(dir);
    return ddList;
}

void printTree(doubleDoubleLinkedListNode* Head) {
    doubleDoubleLinkedListNode* temp1 = Head;
    doubleDoubleLinkedListNode* temp2 = Head;
    while (temp1 != NULL) {
        while (temp2 != NULL) {
            printf("%d  %s \n ", temp2->count, temp2->Filename);
            temp2 = temp2->NextInstanceOfFile;
        }
        temp1 = temp1->next;
        temp2 = temp1;
    }
}

int main(int argc, char* argv[]) {
    doubleDoubleLinkedListNode* ddList = NULL;
    traverseTree(ddList, argv[1]);
    printTree(ddList);
    return 0;
}
*/
