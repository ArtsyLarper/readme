#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <sys/types.h>
#include <regex.h>
#include <unistd.h>
#include <sys/wait.h>
#include <stdbool.h>
#include <limits.h>
#include <libgen.h>
#include <unistd.h>
#include <sys/resource.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/stat.h>

#define URL_PATTERN "<[^>]*href=\"?(https?:[^\"?#]*)[^\"]*\"?[^>]*>"
//#define tofind    "http"

#define file_name ":\\/\\/([^\\/]*)"

const char* urllist[2048];

void check_url(const char *url) {
    pid_t pid = fork();
    int rc, status;
    if (pid == 0) {
        //wget --spider -q --delete-after -T10 -t1 url
        execlp("wget","--spider", "-q", "--delete-after", "-T10", "-t1",  url, NULL);
        perror("exec");
        exit(1);
    } else if (pid < 0) {
        perror("fork");
        exit(1);
    }
    else {
        int status;
        
        waitpid(pid, &status, 0);
    
        if ( WIFEXITED(status) )
        {
            int exit_status = WEXITSTATUS(status);       
            if (exit_status == 0)
            {
                printf ("okay \t %s \n", url); 
            }
            else
            {
                printf("error \t %s \n", url);
            }
        }
    }
    rc = wait(&status);
    
}

int get_url(const char *url, char* name) {
    pid_t pid = fork();
    int rc, status;
    if (pid == 0) {
        //wget --spider -q --delete-after -T10 -t1 url
        execlp("wget", "-q","-o","/dev/null", "-T10", "-t1", "-O", name,  url, NULL);
        perror("exec");
        exit(1);
    } else if (pid < 0) {
        perror("fork");
        exit(1);
    }
    else {
        int status;
        
        waitpid(pid, &status, 0);
    
        if ( WIFEXITED(status) )
        {
            int exit_status = WEXITSTATUS(status);       
            return exit_status;
        }
    }
    return -1;
    
}

static int myCompare(const void* a, const void* b)
{
 
     return strcmp(*(const char**)a, *(const char**)b);
}
 
void sort(const char* arr[], int n)
{
    qsort(arr, n, sizeof(const char*), myCompare);
}

int open_url(const char *url)
{
    char *command;
    // int input;
    command = malloc (sizeof(char)*100);
    sprintf(command, "wget --no-cache -q -O %s",url);
    // system(command);
    popen(command, "r");
}

int main(int argc, char **argv)
{
    struct rlimit core_limit;
    core_limit.rlim_cur = RLIM_INFINITY;
    core_limit.rlim_max = RLIM_INFINITY;
    setrlimit(RLIMIT_CORE, &core_limit); 
    FILE *fp;
    char *line = NULL; // Initialize line to NULL
    size_t len = 0;
    ssize_t read;
    regex_t re;
    regex_t namepicker;
    char *filename = NULL;
    const char *default_filename = "index.html";
    char* name = strdup(argv[1]);
    regmatch_t filenamematch[2];
    int fileon = 0;
    int opt;
    
     while((opt = getopt(argc, argv, "hf:p")) != -1) {
        switch(opt) { 
            case 'p':
                break;
            case 'h':
                printf("get help");
                return 0;
                break;
            case 'f':
                filename = strdup(optarg); // Assign the provided filename to the filename variable
                //fp = fopen(filename, "r");
                printf("%s\n", optarg);
                fileon = 1;
                break;

            case '?':
                break;
        }
    }
    // if (regcomp(&namepicker, file_name, REG_EXTENDED) != 0)
    // {
    //     fprintf(stderr, "Failed to compile regex '%s'\n", file_name);
    //     return EXIT_FAILURE;
    // }
    if (fileon == 0)
    {
        /* code */
    
    
        if (regcomp(&namepicker, file_name, REG_EXTENDED) != 0)
        {
            fprintf(stderr, "Failed to compile regex '%s'\n", file_name);
            regfree(&re); // Free the memory allocated for the URL regex before returning
            return EXIT_FAILURE;
        }


        if (regexec(&namepicker, name, 2, filenamematch, 0) == 0)
        {
            filename = strndup(name + filenamematch[1].rm_so, filenamematch[1].rm_eo - filenamematch[1].rm_so);
            printf("%s\n", filename);
            get_url(argv[1], filename);
        }
        else
        {
        fprintf(stderr, "Failed to extract file name from the provided URL. Using default filename.\n");
            filename = strdup(default_filename);
            get_url(argv[1], filename);
        }

        // regexec(&namepicker, name, 1, filenamematch, 0);
        // char* newname = strndup(name + filenamematch[1].rm_so, filenamematch[1].rm_eo - filenamematch[1].rm_so);
        // printf("%s\n", newname);
        // if (argc > 1)
        //     {
        //         get_url(argv[1], newname);
                
        //     }
    }
        if (regcomp(&re, URL_PATTERN, REG_EXTENDED) != 0)
        {
            fprintf(stderr, "Failed to compile regex '%s'\n", URL_PATTERN);
            return EXIT_FAILURE;
        }

        

        
    
    fp = fopen(filename, "r");
    if (fp == 0)
    {
        fprintf(stderr, "Failed to open file %s (%d: %s)\n",
                filename, errno, strerror(errno));
        return EXIT_FAILURE;
    }
    int i =0;
    regmatch_t match[2];
    while ((read = getline(&line, &len, fp)) != -1) {
        char *cursor = line;
        while (regexec(&re, cursor, 2, match, 0) == 0) {
            urllist[i] = strndup(cursor + match[1].rm_so, match[1].rm_eo - match[1].rm_so);
            cursor += match[0].rm_eo;
            i++;
        }
    }
    sort(urllist, i);
    for (int t= 0; t < i; t++)
    {
        check_url(urllist[t]);
    }
    
    regfree(&re); // Free the memory allocated for the regex
    fclose(fp);
    free(line);
    return EXIT_SUCCESS;
}


