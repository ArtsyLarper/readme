<!DOCTYPE html>
<html lang="en-US" class="no-js">
	<head>	<meta charset="UTF-8">
	<meta name="description" content="">
	
		
			
		<link rel="icon" href="http://www.skindeepcomic.com/wp-content/uploads/2017/01/favicon.png">
		<link rel="apple-touch-icon" href="http://www.skindeepcomic.com/wp-content/uploads/2017/01/favicon.png">
		<link rel="msapplication-TileImage" href="http://www.skindeepcomic.com/wp-content/uploads/2017/01/favicon.png">
		
		
	<link rel="pingback" href="http://www.skindeepcomic.com/xmlrpc.php">
	<title>Skin Deep</title>
<meta name='robots' content='max-image-preview:large' />
<link rel='dns-prefetch' href='//www.skindeepcomic.com' />
<link rel='dns-prefetch' href='//s.w.org' />
<link rel='dns-prefetch' href='//c0.wp.com' />
<link rel="alternate" type="application/rss+xml" title="Skin Deep &raquo; Feed" href="http://www.skindeepcomic.com/feed/" />
<link rel="alternate" type="application/rss+xml" title="Skin Deep &raquo; Comments Feed" href="http://www.skindeepcomic.com/comments/feed/" />
		<script type="text/javascript">
			window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/13.0.1\/72x72\/","ext":".png","svgUrl":"https:\/\/s.w.org\/images\/core\/emoji\/13.0.1\/svg\/","svgExt":".svg","source":{"concatemoji":"http:\/\/www.skindeepcomic.com\/wp-includes\/js\/wp-emoji-release.min.js?ver=5.7.8"}};
			!function(e,a,t){var n,r,o,i=a.createElement("canvas"),p=i.getContext&&i.getContext("2d");function s(e,t){var a=String.fromCharCode;p.clearRect(0,0,i.width,i.height),p.fillText(a.apply(this,e),0,0);e=i.toDataURL();return p.clearRect(0,0,i.width,i.height),p.fillText(a.apply(this,t),0,0),e===i.toDataURL()}function c(e){var t=a.createElement("script");t.src=e,t.defer=t.type="text/javascript",a.getElementsByTagName("head")[0].appendChild(t)}for(o=Array("flag","emoji"),t.supports={everything:!0,everythingExceptFlag:!0},r=0;r<o.length;r++)t.supports[o[r]]=function(e){if(!p||!p.fillText)return!1;switch(p.textBaseline="top",p.font="600 32px Arial",e){case"flag":return s([127987,65039,8205,9895,65039],[127987,65039,8203,9895,65039])?!1:!s([55356,56826,55356,56819],[55356,56826,8203,55356,56819])&&!s([55356,57332,56128,56423,56128,56418,56128,56421,56128,56430,56128,56423,56128,56447],[55356,57332,8203,56128,56423,8203,56128,56418,8203,56128,56421,8203,56128,56430,8203,56128,56423,8203,56128,56447]);case"emoji":return!s([55357,56424,8205,55356,57212],[55357,56424,8203,55356,57212])}return!1}(o[r]),t.supports.everything=t.supports.everything&&t.supports[o[r]],"flag"!==o[r]&&(t.supports.everythingExceptFlag=t.supports.everythingExceptFlag&&t.supports[o[r]]);t.supports.everythingExceptFlag=t.supports.everythingExceptFlag&&!t.supports.flag,t.DOMReady=!1,t.readyCallback=function(){t.DOMReady=!0},t.supports.everything||(n=function(){t.readyCallback()},a.addEventListener?(a.addEventListener("DOMContentLoaded",n,!1),e.addEventListener("load",n,!1)):(e.attachEvent("onload",n),a.attachEvent("onreadystatechange",function(){"complete"===a.readyState&&t.readyCallback()})),(n=t.source||{}).concatemoji?c(n.concatemoji):n.wpemoji&&n.twemoji&&(c(n.twemoji),c(n.wpemoji)))}(window,document,window._wpemojiSettings);
		</script>
		<style type="text/css">
img.wp-smiley,
img.emoji {
	display: inline !important;
	border: none !important;
	box-shadow: none !important;
	height: 1em !important;
	width: 1em !important;
	margin: 0 .07em !important;
	vertical-align: -0.1em !important;
	background: none !important;
	padding: 0 !important;
}
</style>
	<link rel='stylesheet' id='wp-block-library-css'  href='https://c0.wp.com/c/5.7.8/wp-includes/css/dist/block-library/style.min.css' type='text/css' media='all' />
<style id='wp-block-library-inline-css' type='text/css'>
.has-text-align-justify{text-align:justify;}
</style>
<link rel='stylesheet' id='mediaelement-css'  href='https://c0.wp.com/c/5.7.8/wp-includes/js/mediaelement/mediaelementplayer-legacy.min.css' type='text/css' media='all' />
<link rel='stylesheet' id='wp-mediaelement-css'  href='https://c0.wp.com/c/5.7.8/wp-includes/js/mediaelement/wp-mediaelement.min.css' type='text/css' media='all' />
<link rel='stylesheet' id='inkblot-theme-css'  href='http://www.skindeepcomic.com/wp-content/themes/inkblot/style.css?ver=5.7.8' type='text/css' media='all' />
<style id='inkblot-theme-inline-css' type='text/css'>
.sidebar1{width:23%}.sidebar2{width:25%}.sidebar3{width:25%}main{width:calc(77% - 2px)}.wrapper{min-width:1025px;max-width:1025px;width:1025px;background-color:#ded3a5;color:#5f6034}.document-header{min-width:1025px;max-width:1025px;width:1025px}.document-footer{min-width:1025px;max-width:1025px;width:1025px}body{background-color:#5f6034;color:#ded3a5}input{background-color:#ded3a5;color:#5f6034;border-color:#5f6034}textarea{background-color:#ded3a5;color:#5f6034;border-color:#5f6034}.banner nav{background-color:#5f6034;color:#ded3a5}.banner ul ul{background-color:#5f6034;color:#ded3a5}.banner select{background-color:#5f6034;color:#ded3a5}.contentinfo{background-color:#5f6034;color:#ded3a5}.post-webcomic nav{background-color:#5f6034;color:#ded3a5}button{background-color:#5f6034;color:#ded3a5}input[type="submit"]{background-color:#5f6034;color:#ded3a5}input[type="reset"]{background-color:#5f6034;color:#ded3a5}input[type="button"]{background-color:#5f6034;color:#ded3a5}a{color:#ded3a5}a:focus{color:#ffffff}a:hover{color:#ffffff}button:focus{background-color:#5f6034}button:hover{background-color:#5f6034}input[type="submit"]:focus{background-color:#5f6034}input[type="submit"]:hover{background-color:#5f6034}input[type="reset"]:focus{background-color:#5f6034}input[type="reset"]:hover{background-color:#5f6034}input[type="button"]:focus{background-color:#5f6034}input[type="button"]:hover{background-color:#5f6034}.wrapper a{color:#5f6034}.post-footer span{color:#5f6034}nav.pagination{color:#5f6034}blockquote{border-color:#5f6034}hr{border-color:#5f6034}pre{border-color:#5f6034}th{border-color:#5f6034}td{border-color:#5f6034}fieldset{border-color:#5f6034}.post-footer{border-color:#5f6034}.comment{border-color:#5f6034}.comment .comment{border-color:#5f6034}.pingback{border-color:#5f6034}.trackback{border-color:#5f6034}.bypostauthor{border-color:#5f6034}.wrapper a:focus{color:#ffffff}.wrapper a:hover{color:#ffffff}input:focus{border-color:#ffffff}input:hover{border-color:#ffffff}textarea:focus{border-color:#ffffff}textarea:hover{border-color:#ffffff}.banner nav:before{color:#ded3a5}.banner nav a{color:#ded3a5}.contentinfo a{color:#ded3a5}.post-webcomic nav a{color:#ded3a5}.banner h1{display:none;visibility:hidden}.banner p{display:none;visibility:hidden}
</style>
<link rel='stylesheet' id='jetpack_css-css'  href='https://c0.wp.com/p/jetpack/9.9.1/css/jetpack.css' type='text/css' media='all' />
<script type='text/javascript' src='https://c0.wp.com/c/5.7.8/wp-includes/js/jquery/jquery.min.js' id='jquery-core-js'></script>
<script type='text/javascript' src='https://c0.wp.com/c/5.7.8/wp-includes/js/jquery/jquery-migrate.min.js' id='jquery-migrate-js'></script>
<script type='text/javascript' src='http://www.skindeepcomic.com/wp-content/plugins/wp-ajax-edit-comments/js/jquery.colorbox.min.js?ver=6.1' id='colorbox-js'></script>
<link rel="https://api.w.org/" href="http://www.skindeepcomic.com/wp-json/" /><link rel="EditURI" type="application/rsd+xml" title="RSD" href="http://www.skindeepcomic.com/xmlrpc.php?rsd" />
<link rel="wlwmanifest" type="application/wlwmanifest+xml" href="http://www.skindeepcomic.com/wp-includes/wlwmanifest.xml" /> 
<meta name="generator" content="WordPress 5.7.8" />
<!-- WP Favicon -->
<link rel="shortcut icon" href="http://www.skindeepcomic.com/favicon.ico" type="image/x-icon" />
<link rel="icon"          href="http://www.skindeepcomic.com/favicon.gif" type="image/gif"    />
<!-- /WP Favicon -->
<style type='text/css'>img#wpstats{display:none}</style>
				<style type="text/css" id="wp-custom-css">
			body{padding-bottom:0;padding-top:0;}.wrapper {padding: 0;}.banner nav a {font-weight: bold; padding-left: 0.8rem; padding-right: 0.8rem;}.post-webcomic nav {background:#ded3a5;}.post-webcomic nav a {color:#5f6034;display:block}.post-webcomic nav a:hover,.post-webcomic nav a:focus {background: #5f6034}.post-webcomic nav .current-webcomic {display:none}.banner nav li li {float: none;}.banner nav li:focus > a, .banner nav li:hover > a {background: #ded3a5; color: #5f6034;}		</style>
		</head>
	<body id="document" class="home blog custom-background two-column content-left">
		<a href="#content">Skip to content</a>
		
				
		<div class="wrapper">
			
						
			<header role="banner" class="banner widgets columns-1">
				
									
					<a href="http://www.skindeepcomic.com" rel="home">
						<h1 class="site">Skin Deep</h1>
						<p></p>
						
													
							<img src="http://www.skindeepcomic.com/wp-content/uploads/2011/08/wordpresslogo10251.jpg" width="1025" height="150" alt="Skin Deep">
							
												
					</a>
					
					<nav role="navigation" aria-label="Primary Navigation">
						
						<ul id="menu-primary" class="menu"><li id="menu-item-6044" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-6044"><a href="http://korybing.storenvy.com">Store!</a></li>
<li id="menu-item-6045" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-6045"><a href="http://www.skindeepcomic.com/about/">About</a></li>
<li id="menu-item-6052" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-6052"><a href="http://www.skindeepcomic.com/archives/">Chapters</a>
<ul class="sub-menu">
	<li id="menu-item-6083" class="menu-item menu-item-type-post_type menu-item-object-webcomic1 menu-item-has-children menu-item-6083"><a href="http://www.skindeepcomic.com/archive/issue-1-cover/">Orientations</a>
	<ul class="sub-menu">
		<li id="menu-item-6084" class="menu-item menu-item-type-post_type menu-item-object-webcomic1 menu-item-6084"><a href="http://www.skindeepcomic.com/archive/issue-1-cover/">Part I</a></li>
		<li id="menu-item-6085" class="menu-item menu-item-type-post_type menu-item-object-webcomic1 menu-item-6085"><a href="http://www.skindeepcomic.com/archive/issue-two-cover/">Part II</a></li>
		<li id="menu-item-6086" class="menu-item menu-item-type-post_type menu-item-object-webcomic1 menu-item-6086"><a href="http://www.skindeepcomic.com/archive/issue-three-cover/">Part III</a></li>
		<li id="menu-item-6087" class="menu-item menu-item-type-post_type menu-item-object-webcomic1 menu-item-6087"><a href="http://www.skindeepcomic.com/archive/issue-four-cover/">Part IV</a></li>
		<li id="menu-item-6088" class="menu-item menu-item-type-post_type menu-item-object-webcomic1 menu-item-6088"><a href="http://www.skindeepcomic.com/archive/issue-five-cover/">Part V</a></li>
	</ul>
</li>
	<li id="menu-item-6094" class="menu-item menu-item-type-post_type menu-item-object-webcomic1 menu-item-6094"><a href="http://www.skindeepcomic.com/archive/fiddlers-cave-1/">Fiddler&#8217;s Cave</a></li>
	<li id="menu-item-6063" class="menu-item menu-item-type-post_type menu-item-object-webcomic1 menu-item-has-children menu-item-6063"><a href="http://www.skindeepcomic.com/archive/handshakes-1-introduction/">Exchanges</a>
	<ul class="sub-menu">
		<li id="menu-item-6090" class="menu-item menu-item-type-post_type menu-item-object-webcomic1 menu-item-6090"><a href="http://www.skindeepcomic.com/archive/handshakes-1-introduction/">Handshakes</a></li>
		<li id="menu-item-6091" class="menu-item menu-item-type-post_type menu-item-object-webcomic1 menu-item-6091"><a href="http://www.skindeepcomic.com/archive/heartaches-1-hello-missus-gillis/">Heartaches</a></li>
		<li id="menu-item-6093" class="menu-item menu-item-type-post_type menu-item-object-webcomic1 menu-item-6093"><a href="http://www.skindeepcomic.com/archive/exchanges-hello-goodbye/">Hello, Goodbye</a></li>
	</ul>
</li>
	<li id="menu-item-6095" class="menu-item menu-item-type-post_type menu-item-object-webcomic1 menu-item-6095"><a href="http://www.skindeepcomic.com/archive/one-eyed-bear-1/">The One-Eyed Bear</a></li>
	<li id="menu-item-6096" class="menu-item menu-item-type-post_type menu-item-object-webcomic1 menu-item-6096"><a href="http://www.skindeepcomic.com/archive/ridiculous-creatures-1/">Ridiculous Creatures</a></li>
	<li id="menu-item-6097" class="menu-item menu-item-type-post_type menu-item-object-webcomic1 menu-item-6097"><a href="http://www.skindeepcomic.com/archive/nixie-spit/">Nixie Spit</a></li>
	<li id="menu-item-6098" class="menu-item menu-item-type-post_type menu-item-object-webcomic1 menu-item-6098"><a href="http://www.skindeepcomic.com/archive/homecoming-1-chotchkies/">Homecoming</a></li>
	<li id="menu-item-6099" class="menu-item menu-item-type-post_type menu-item-object-webcomic1 menu-item-6099"><a href="http://www.skindeepcomic.com/archive/the-bugbear-talisman/">The Bugbear Talisman</a></li>
	<li id="menu-item-6100" class="menu-item menu-item-type-post_type menu-item-object-webcomic1 menu-item-6100"><a href="http://www.skindeepcomic.com/archive/greetings-from-dogpatch-page-1/">Greetings from Dogpatch</a></li>
	<li id="menu-item-6103" class="menu-item menu-item-type-post_type menu-item-object-webcomic1 menu-item-6103"><a href="http://www.skindeepcomic.com/archive/kill-them-with-kindness-1-omg-shoes/">Kill Them with Kindness</a></li>
	<li id="menu-item-6104" class="menu-item menu-item-type-post_type menu-item-object-webcomic1 menu-item-6104"><a href="http://www.skindeepcomic.com/archive/reunion-page-1-the-noir-house/">Reunion</a></li>
	<li id="menu-item-6181" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-6181"><a target="_blank" rel="noopener" href="http://www.skindeepcomic.com/archive/illumination-page-1/#new_tab">Illumination</a>
	<ul class="sub-menu">
		<li id="menu-item-6188" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-6188"><a href="http://www.skindeepcomic.com/archive/illumination-page-1/">Chapter I</a></li>
		<li id="menu-item-6187" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-6187"><a href="http://www.skindeepcomic.com/archive/illumination-ch2-page-1/">Chapter II</a></li>
		<li id="menu-item-7028" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-7028"><a href="http://www.skindeepcomic.com/archive/illumination-chapter-3-page-1">Chapter III</a></li>
	</ul>
</li>
	<li id="menu-item-7781" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-7781"><a href="http://www.skindeepcomic.com/archive/obverse-reverse-chapter-1-claws-that-catch/">Obverse &#038; Reverse</a>
	<ul class="sub-menu">
		<li id="menu-item-7975" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-7975"><a href="http://www.skindeepcomic.com/archive/obverse-reverse-chapter-1-claws-that-catch/">Chapter 1</a></li>
		<li id="menu-item-7978" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-7978"><a href="http://www.skindeepcomic.com/archive/obverse-reverse-chapter-2-within-a-forest-dark/">Chapter 2</a></li>
		<li id="menu-item-7995" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-7995"><a href="http://www.skindeepcomic.com/archive/obverse-reverse-chapter-3-curiouser-curiouser/">Chapter 3</a></li>
		<li id="menu-item-8268" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-8268"><a href="http://www.skindeepcomic.com/archive/obverse-reverse-chapter-4-page-1/">Chapter 4</a></li>
		<li id="menu-item-8540" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-8540"><a href="http://www.skindeepcomic.com/archive/obverse-reverse-chapter-5-were-all-mad-here/">Chapter 5</a></li>
		<li id="menu-item-8608" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-8608"><a href="http://www.skindeepcomic.com/archive/obverse-reverse-chapter-6-abandon-all-hope/">Chapter 6</a></li>
		<li id="menu-item-8784" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-8784"><a target="_blank" rel="noopener" href="http://www.skindeepcomic.com/archive/obverse-reverse-chapter-7-page-1/#new_tab">Chapter 7</a></li>
	</ul>
</li>
</ul>
</li>
<li id="menu-item-6182" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-6182"><a href="http://www.skindeepcomic.com/characters/">Characters</a></li>
<li id="menu-item-6047" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-6047"><a href="http://www.skindeepcomic.com/rq-main/">Reader Questions</a></li>
<li id="menu-item-6046" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-6046"><a href="http://www.skindeepcomic.com/extras/">Community &#038; Extras</a>
<ul class="sub-menu">
	<li id="menu-item-6049" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-6049"><a href="http://skindeep.wikia.com">Skin Deep Wiki!</a></li>
	<li id="menu-item-6050" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-6050"><a href="http://www.reddit.com/r/skindeepcomic/">/r/skindeepcomic</a></li>
	<li id="menu-item-7064" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-7064"><a href="https://discord.gg/xJBKvK8">Discord Avalon</a></li>
	<li id="menu-item-6048" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-6048"><a href="http://skindeepcomic.tumblr.com">Tumblr!</a></li>
</ul>
</li>
</ul>						
					</nav>
					
								
			</header><!-- .banner -->
			<div id="content" class="content" tabindex="-1">
				
				


<main role="main">
	
	<div class="post-webcomic" data-webcomic-container data-webcomic-shortcuts data-webcomic-gestures>
	
		
		
	<div class="webcomic-image scroll">
		
		<a href='http://www.skindeepcomic.com/archive/obverse-reverse-chapter-7-page-67/' class='webcomic-link webcomic1-link next-webcomic-link next-webcomic1-link current-webcomic current-webcomic1'><img width="750" height="1125" src="http://www.skindeepcomic.com/wp-content/uploads/2023/04/sdor7-67.jpg" class="attachment-full size-full" alt="" loading="lazy" srcset="http://www.skindeepcomic.com/wp-content/uploads/2023/04/sdor7-67.jpg 750w, http://www.skindeepcomic.com/wp-content/uploads/2023/04/sdor7-67-200x300.jpg 200w, http://www.skindeepcomic.com/wp-content/uploads/2023/04/sdor7-67-683x1024.jpg 683w, http://www.skindeepcomic.com/wp-content/uploads/2023/04/sdor7-67-96x144.jpg 96w" sizes="(max-width: 750px) 100vw, 750px" /></a>		
	</div><!-- .webcomic-image -->
	
			
		<nav role="navigation" class="widgets columns-5 below" aria-label="Webcomic Navigation Footer">
			
			<aside><a href='http://www.skindeepcomic.com/archive/issue-1-cover/' class='webcomic-link webcomic1-link first-webcomic-link first-webcomic1-link'>&laquo;</a></aside><aside><a href='http://www.skindeepcomic.com/archive/obverse-reverse-chapter-7-page-66/' class='webcomic-link webcomic1-link previous-webcomic-link previous-webcomic1-link'>&lsaquo;</a></aside><aside><a href='http://www.skindeepcomic.com/archive/heartaches-13-magic/' class='webcomic-link webcomic1-link random-webcomic-link random-webcomic1-link'>&infin;</a></aside><aside><a href='http://www.skindeepcomic.com/archive/obverse-reverse-chapter-7-page-67/' class='webcomic-link webcomic1-link next-webcomic-link next-webcomic1-link current-webcomic current-webcomic1'>&rsaquo;</a></aside><aside><a href='http://www.skindeepcomic.com/archive/obverse-reverse-chapter-7-page-67/' class='webcomic-link webcomic1-link last-webcomic-link last-webcomic1-link current-webcomic current-webcomic1'>&raquo;</a></aside>			
		</nav><!-- .widgets.below -->
		
		
		
</div><!-- .post-webcomic -->
<article role="article" id="post-10449" class="post-10449 webcomic1 type-webcomic1 status-publish hentry category-skin-deep webcomic1_storyline-obversereverse webcomic-media-1">
	
		
		
	<header class="post-header">
		
					
			<h2><a href="http://www.skindeepcomic.com/archive/obverse-reverse-chapter-7-page-67/" rel="bookmark">Obverse &#038; Reverse Chapter 7 Page 67</a></h2>
			
				
		<div class="post-details">
			
			<a href="http://www.skindeepcomic.com/archive/obverse-reverse-chapter-7-page-67/" rel="bookmark"><span class="screen-reader-text">Obverse &#038; Reverse Chapter 7 Page 67 published on </span><time datetime="2023-04-25T00:00:27-07:00">April 25, 2023</time></a><a href="http://www.skindeepcomic.com/author/admin/" rel="author"><span class="screen-reader-text">Read more posts by the author of Obverse &#038; Reverse Chapter 7 Page 67, </span>KoryBing</a><a href="http://www.skindeepcomic.com/archive/obverse-reverse-chapter-7-page-67/#comments">17 Comments<span class="screen-reader-text"> on Obverse &#038; Reverse Chapter 7 Page 67</span></a>			
		</div>
	</header><!-- .post-header -->
	
			
		<div class="post-content">
			
			<p>I&#8217;ll be at TCAF this weekend! If you&#8217;re in Toronto, come by and say hello! It&#8217;s a free show in the Toronto Public Library, it&#8217;s great time!<br />
<img loading="lazy" class="aligncenter size-full wp-image-10450" src="http://www.skindeepcomic.com/wp-content/uploads/2023/04/tcafmap.gif" alt="" width="750" height="497" /></p>
			
		</div>
		
		
	<footer class="post-footer">
		
		<span class="post-categories"><span class="screen-reader-text">Categories </span><a href="http://www.skindeepcomic.com/category/skin-deep/" rel="tag">Skin Deep</a></span><span class="webcomic-collections"><span class="screen-reader-text">Webcomic Collections</span><a href='http://www.skindeepcomic.com/skindeep/' class='webcomic-collection-link webcomic1-collection-link current-webcomic-collection'>Skin Deep</a></span><span class="webcomic-storylines"><span class="screen-reader-text">Webcomic Storylines</span><a href='http://www.skindeepcomic.com/skindeep-storyline/obversereverse/' class='webcomic-term-link webcomic1_storyline-link self-webcomic-term-link self-webcomic1_storyline-link'>Obverse &amp; Reverse</a></span>		
	</footer><!-- .post-footer -->
	
</article><!-- #post-10449 -->

<div class="post-content">
	<p>
		
				
	</p>
	
	
</div><!-- .page-content -->	
</main>


	
	<div class="sidebar1 widgets columns-1">
		<h1 class="screen-reader-text">Primary Sidebar</h1>
		
		<aside id="text-33" class="widget widget_text"><h2>Updates Every Tuesday!</h2>			<div class="textwidget"><b>2024 Convention Schedule:</b><br>
<a href="https://www.emeraldcitycomiccon.com/"><b>ECCC:</b></a> March 2-5, Seattle.<br>
<a href="https://www.torontocomics.com/"><b>TCAF:</b></a> April 29-30, Toronto<br>
<b><a href="https://www.vancaf.org/">VanCAF:</a></b> May 20-21, Vancouver<br>
<b><a href="https://www.comic-con.org/cci">SDCC:</a></b> July 20-23, San Diego<br>
<b><a href="https://visioncon.net/">VisionCon:</b></a> October 13-15, Springfield, Missouri</div>
		</aside><aside id="text-17" class="widget widget_text"><h2>New Reader? Start Here!</h2>			<div class="textwidget"><a href="http://www.skindeepcomic.com/archive/issue-1-cover/"><img src="http://www.skindeepcomic.com/wp-content/uploads/2011/08/navOrientations.jpg"></a><br>
Current Story:
<a href="http://www.skindeepcomic.com/archive/obverse-reverse-chapter-1-claws-that-catch/"><img src="http://www.skindeepcomic.com/wp-content/uploads/2019/01/obversereversebutton.jpg"></a><br>
Latest Chapter:
<a href="http://www.skindeepcomic.com/archive/obverse-reverse-chapter-7-page-1/"><img src="http://www.skindeepcomic.com/wp-content/uploads/2022/01/chapter7button.jpg"></a></div>
		</aside><aside id="text-26" class="widget widget_text">			<div class="textwidget"><a href="https://www.patreon.com/korybing"><img src="http://www.skindeepcomic.com/wp-content/uploads/2021/03/pinclubbutton.gif" style="padding-top: 10%;"></a>

	<a href="https://topatoco.com/collections/korybing" onClick="_gaq.push(['_trackEvent', 'External Link', 'Store', 'Sidebar',, false]);" target="_blank" rel="noopener"><img src="http://www.skindeepcomic.com/wp-content/uploads/2020/02/topatocoannouncementsmall.png" style="padding-top: 10%;"><br></a>

	<a href="http://korybing.storenvy.com/collections/247195-books" onClick="_gaq.push(['_trackEvent', 'External Link', 'Store', 'Sidebar',, false]);" target="_blank" rel="noopener"><img src="http://www.skindeepcomic.com/wp-content/uploads/2013/10/skindeepstore5.png" style="padding-top: 10%;"><br></a>	


<a href="http://www.patreon.com/korybing" onClick="_gaq.push(['_trackEvent', 'External Link', 'Store', 'Sidebar',, false]);" target="_blank" rel="noopener"><img src="http://www.skindeepcomic.com/wp-content/uploads/2014/02/patreonbutton.jpg"></a><br>

<a href="https://knifebeetle.neocities.org/"><img src="http://www.skindeepcomic.com/wp-content/uploads/2023/02/kb2-none.png"></a></div>
		</aside>	
		
	</div><!-- .sidebar1 -->
	


							
			<div class="widgets content-footer columns-1 ">
				<h1 class="screen-reader-text">Content Footer</h1>
				
				<aside id="text-7" class="widget widget_text">			<div class="textwidget"><p><center><font size="-2">Art and Story © <a href="http://www.korybing.com">Kory Bingaman Francka</a> 2006–2020<br />
Anthony Gillis, Blanche Noir, Rupert Burton-Fitzgerald, Phoenix, Royce Carmikal, other noted miscellaneous characters created by <a href="http://sfemonster.com">Sfé R. Monster</a>.<br />
Alec Hyde, Ike Sanford, Rhonda Phelton, Sam Hain, other noted miscellaneous characters created by <a href="http://sheanamolloy.com/">Shean Molloy</a>.</font></center></p>
</div>
		</aside>				
			</div><!-- #content-footer -->
			
						
			</div><!-- #content.content -->
			<footer role="contentinfo" class="contentinfo widgets columns-1">
				
				<a href="#document">&copy; 2006&ndash;2023 Skin Deep</a> &bull; Powered by <a href="//wordpress.org">WordPress</a> with <a href="//github.com/mgsisk/inkblot">Inkblot</a>				
			</footer><!-- .contentinfo -->
			
						
		</div><!-- .wrapper -->
		
		<script type='text/javascript' id='Mgsisk\Webcomic\CollectionCommonJS-js-extra'>
/* <![CDATA[ */
var webcomicCommonJS = {"ajaxurl":"http:\/\/www.skindeepcomic.com\/wp-admin\/admin-ajax.php"};
/* ]]> */
</script>
<script type='text/javascript' src='http://www.skindeepcomic.com/wp-content/plugins/webcomic/srv/collection/common.js?ver=5.0.6' id='Mgsisk\Webcomic\CollectionCommonJS-js'></script>
<script type='text/javascript' id='Mgsisk\Webcomic\TranscribeCommonJS-js-extra'>
/* <![CDATA[ */
var webcomicCommonJS = {"ajaxurl":"http:\/\/www.skindeepcomic.com\/wp-admin\/admin-ajax.php"};
/* ]]> */
</script>
<script type='text/javascript' src='http://www.skindeepcomic.com/wp-content/plugins/webcomic/srv/transcribe/common.js?ver=5.0.6' id='Mgsisk\Webcomic\TranscribeCommonJS-js'></script>
<script type='text/javascript' src='http://www.skindeepcomic.com/wp-content/themes/inkblot/-/js/script.js?ver=5.7.8' id='inkblot-script-js'></script>
<script type='text/javascript' src='http://www.skindeepcomic.com/wp-content/plugins/page-links-to/dist/new-tab.js?ver=3.3.5' id='page-links-to-js'></script>
<script type='text/javascript' id='aec_atd-js-extra'>
/* <![CDATA[ */
var aec_frontend = {"atdlang":"true","atd":"true","expand":"true","url":"http:\/\/www.skindeepcomic.com\/?aec_page=comment-popup.php","title":"Comment Box"};
/* ]]> */
</script>
<script type='text/javascript' src='http://www.skindeepcomic.com/wp-content/plugins/wp-ajax-edit-comments/js/jquery.atd.textarea.js?ver=6.1' id='aec_atd-js'></script>
<script type='text/javascript' src='http://www.skindeepcomic.com/wp-content/plugins/wp-ajax-edit-comments/js/frontend.js?ver=6.1' id='aec_frontend-js'></script>
<script type='text/javascript' src='https://c0.wp.com/c/5.7.8/wp-includes/js/wp-embed.min.js' id='wp-embed-js'></script>
<script src='https://stats.wp.com/e-202317.js' defer></script>
<script>
	_stq = window._stq || [];
	_stq.push([ 'view', {v:'ext',j:'1:9.9.1',blog:'17409883',post:'0',tz:'-7',srv:'www.skindeepcomic.com'} ]);
	_stq.push([ 'clickTrackerInit', '17409883', '0' ]);
</script>
		
	</body><!-- #document -->
</html>