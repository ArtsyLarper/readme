#include <stdlib.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <limits.h>
#include <errno.h>
#include <libgen.h>
#include <unistd.h>
#include <sys/resource.h>
#include <signal.h>
#include<sys/wait.h>

int* createcore (void)
{
    //printf("before fork");
    pid_t child_pid = fork();
    if(child_pid == 0)
    {
        //printf("child");
        raise(SIGSEGV);
        kill(getpid(),SIGKILL);
    }
    // else
    // {
    //     printf("parent");
    //     wait(NULL);
    // }
    int *stuff;
    stuff = NULL;
    return stuff;
    
}

extern int snapshot(char *fn, char *progpath, char *readme)
{
    struct rlimit core_limit;
    core_limit.rlim_cur = RLIM_INFINITY;
    core_limit.rlim_max = RLIM_INFINITY;
    setrlimit(RLIMIT_CORE, &core_limit);        
    wait(createcore());
    int procid;
    procid = getpid();
    char exename [PATH_MAX];
    sprintf(exename, "/proc/%d/exe", procid);
    FILE* exeFile;
    exeFile = fopen(exename, "rb");
    int ssnamedir;
    ssnamedir = mkdir(fn , S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
    if (ssnamedir == -1)
    {
        printf("couldn't create directory %s %d\n", fn, errno);
        if (errno == 17)
        {
            printf("Error directory already exists!\n");
        }
        return -1;
    }
    FILE * pFILE1;
    FILE * pFILE2;
    FILE * pFILE3;

    char name1 [PATH_MAX];
    sprintf(name1,"%s/%s",fn,basename(progpath));
    
    char name2 [PATH_MAX];
    sprintf(name2,"%s/core",fn);
    
    char name3 [PATH_MAX];
    sprintf(name3,"%s/README.txt",fn);

    //program name
    pFILE1 = fopen(name1, "wb");
    if (pFILE1 == NULL)
    {
        printf("couldn't create executable %s %d\n", name1, errno);
        return -1;
    }

    size_t n, m;
    unsigned char buff[8192];
    do {
        n = fread(buff, 1, sizeof buff, exeFile);
        if (n) m = fwrite(buff, 1, n, pFILE1);
        else   m = 0;
        } while ((n > 0) && (n == m));
    if (m) perror("copy");

    fclose(pFILE1);
    fclose(exeFile);

    //core
    pFILE2 = fopen(name2, "w");
    FILE *coredump;
    coredump = fopen("core", "r");
    
    if (pFILE2 == NULL || coredump == NULL)
    {
        printf("couldn't create core dump %s %d\n", name2, errno);
        return -1;
    }
    unsigned char buff2[8192];
    do {
        n = fread(buff2, 1, sizeof buff2, coredump);
        if (n) m = fwrite(buff2, 1, n, pFILE2);
        else   m = 0;
        } while ((n > 0) && (n == m));
    if (m) perror("copy");

    // while( ( ch = fgetc(coredump) ) != EOF )
    // {
    //     fputc(ch, pFILE2);
    // }
    fclose(pFILE2);

    //readme
    pFILE3 = fopen (name3,"w");
    if (pFILE3 == NULL)
    {
        printf("couldn't create readme %s %d\n", name3, errno);
        return -1;
    }
    fprintf (pFILE3, "%s\n", readme);
    
    struct stat info;
stat(name1, &info);
    //printf("original permissions were: %08x\n", info.st_mode);
    if (chmod(name1, S_IRWXU|S_IRWXG|S_IRWXO) != 0)
      perror("chmod() error");
    else {
      stat(name1, &info);
      //printf("after chmod(), permissions are: %08x\n", info.st_mode);
    }
    

    //chmod(pFILE1, S_IRWXU|S_IRWXG|S_IRWXO);
    fclose (pFILE3);
    char command [PATH_MAX];
    sprintf(command,"tar czvf %s.tgz %s",fn, fn);
    system(command);
    unlink(name1);
    unlink(name2);
    unlink(name3);
    rmdir(fn);
    unlink("core");
    return 0;
}


/*
#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <limits.h>
#include <errno.h>
#include <libgen.h>
#include <unistd.h>
#include <sys/resource.h>

int copy_file(const char *src, const char *dst)
{
    FILE *src_file = fopen(src, "rb");
    if (!src_file)
    {
        perror("fopen src");
        return -1;
    }

    FILE *dst_file = fopen(dst, "wb");
    if (!dst_file)
    {
        perror("fopen dst");
        fclose(src_file);
        return -1;
    }

    unsigned char buff[8192];
    size_t n, m;
    do {
        n = fread(buff, 1, sizeof(buff), src_file);
        if (n) m = fwrite(buff, 1, n, dst_file);
        else   m = 0;
    } while ((n > 0) && (n == m));

    fclose(src_file);
    fclose(dst_file);

    if (n > 0 || ferror(src_file) || ferror(dst_file))
    {
        perror("copy_file");
        return -1;
    }

    return 0;
}

int snapshot(char *fn, char *progpath, char *readme)
{
    struct rlimit core_limit;
    core_limit.rlim_cur = RLIM_INFINITY;
    core_limit.rlim_max = RLIM_INFINITY;
    if (setrlimit(RLIMIT_CORE, &core_limit) == -1)
    {
        perror("setrlimit");
        return -1;
    }

    pid_t procid = getpid();
    char exename[PATH_MAX];
    snprintf(exename, sizeof(exename), "/proc/%d/exe", procid);

    if (mkdir(fn, S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH) == -1 && errno != EEXIST)
    {
        perror("mkdir");
        return -1;
    }

    char name1[PATH_MAX];
    snprintf(name1, sizeof(name1), "%s/%s", fn, basename(progpath));

    char name2[PATH_MAX];
    snprintf(name2, sizeof(name2), "%s/core", fn);

    char name3[PATH_MAX];
    snprintf(name3, sizeof(name3), "%s/readme", fn);

    if (copy_file(exename, name1) == -1)
    {
        fprintf(stderr, "couldn't copy executable %s\n", name1);
        return -1;
    }

    if (access("core", F_OK) != -1)
    {
        if (copy_file("core", name2) == -1)
        {
            fprintf(stderr, "couldn't copy core dump %s\n", name2);
            return -1;
        }
    }

    FILE *pFILE3 = fopen(name3, "w");
    if (!pFILE3)
    {
        perror("fopen readme");
        return -1;
    }
    fprintf(pFILE3, "%s\n", readme);
    fclose(pFILE3);

    char command[PATH_MAX];
    snprintf(command, sizeof(command), "tar czvf %s.tar.gz %s", fn, fn);

    if (system(command) == -1)
    {
        perror("system");
        return -1;
    }

    return 0;
}
*/