#include <stdlib.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <limits.h>
#include <errno.h>
#include <libgen.h>
#include <unistd.h>
#include <sys/resource.h>
#include <signal.h>
#include<sys/wait.h>
#include "tattle.h"

int main(int argc, char **argv)
{
    struct rlimit core_limit;
    core_limit.rlim_cur = RLIM_INFINITY;
    core_limit.rlim_max = RLIM_INFINITY;
    setrlimit(RLIMIT_CORE, &core_limit); 
    tattle(argc, argv);
    return 0;
}
