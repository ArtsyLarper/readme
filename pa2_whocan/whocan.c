#include <sys/types.h>
#include <sys/stat.h>
#include <time.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <pwd.h>
#include <assert.h>
#include <grp.h>
#include <unistd.h>
#include <limits.h>
#include <libgen.h>




int check_parent_dir_permissions(int permission)
{
    char cwd[PATH_MAX];
    char originaldirectory[PATH_MAX];
    getcwd(originaldirectory, sizeof(originaldirectory));
    struct stat sb;
    int i;
    i = 0;
    while (strcmp(getcwd(cwd, sizeof(cwd)), "/") != 0)
    {
    //printf("%s \n", cwd);
        stat(".",&sb);
        if (!(sb.st_mode & permission))
        {
            /* code */
            //printf("%s does not have permission\n", cwd);
            chdir(originaldirectory);
            return 1;
        }
        
        chdir("..");
    }
    chdir(originaldirectory);
    return 0;
}

void printgroups(struct stat sb)
{
 
    struct passwd *pwent;
    long mxnGids = sysconf(_SC_NGROUPS_MAX);
    int nGids;
    gid_t *gids;
    struct group *group;


    gids = (gid_t *) malloc(mxnGids * sizeof(gid_t));

    while ((pwent = getpwent()) != NULL) {
        
        nGids = mxnGids; // maximum possible # of gids (input)
        nGids = getgrouplist(pwent->pw_name, pwent->pw_gid, gids, &nGids);
        assert(nGids > 0);
        for (int i = 0; i < nGids; i++) {
            if (gids[i]==sb.st_gid)
            {   
                printf("%s\n",pwent->pw_name); 
                // group = getgrgid(gids[i]);
                // if (group == NULL)
                //     printf("%d (no group name found)", gids[i]);
                // else if (i == 0)
                //     printf("%d (%s)", gids[i], group->gr_name);
                // else
                //     printf(", %d (%s)", gids[i], group->gr_name);
            }
        }

    }
    //printf("\n");
}

void printowner (struct stat sb)
{
    struct passwd *pwent;
    while ((pwent = getpwent()) != NULL)
    {
        /* code */
        if(sb.st_uid==pwent->pw_uid)
        {
            printf("%s\n",pwent->pw_name);
        }
    }
    
}

int	main(int argc, char **argv)
{
    
    struct stat sb;

   if (argc != 3) {
        fprintf(stderr, "Usage: %s <action> <filename>\n", argv[0]);
        exit(EXIT_FAILURE);
    }

   if (stat(argv[2], &sb) == -1) {
        perror("stat");
        exit(EXIT_FAILURE);
    }

    chdir(dirname(argv[2]));

    if (strcmp(argv[1], "cd")==0)
    {
        if (S_ISDIR(sb.st_mode))
        {
            /* code */
            
            
            if( sb.st_mode & S_IXUSR )
            {
                //printf("owner can cd into directory\n");
                if( sb.st_mode & S_IXGRP && check_parent_dir_permissions(S_IXGRP)==0)
                {
                    //printf("Group can cd into directory\n");
                    if( sb.st_mode & S_IXOTH && check_parent_dir_permissions(S_IXOTH)==0)
                    {
                        printf("(everyone)\n");
                        return 0;
                    }
                    printgroups(sb);
                }
                printowner(sb);
            }

        }
        printf("root\n");
    }

    else if (strcmp(argv[1], "delete")==0)
    {
    
        if( !(sb.st_mode & S_ISVTX) && check_parent_dir_permissions(S_IXOTH)==0 && check_parent_dir_permissions(S_IWOTH)==0)
        {
            printf("(everyone)\n");
                    return 0;
        }
            /* code */
        if( !(sb.st_mode & S_ISVTX) && check_parent_dir_permissions(S_IXGRP)==0 && check_parent_dir_permissions(S_IWGRP)==0)
        {
            //  printf("Group can delete\n");

            printgroups(sb);
            printf("root\n");
            return 0;
        }
        if( check_parent_dir_permissions(S_IXUSR)==0 && check_parent_dir_permissions(S_IWUSR)==0)
        {
            //printf("owner can delete\n");

            printowner(sb);
            
        }
        
        printf("root\n");
        //return 0;

        
    }

   else if (strcmp(argv[1], "execute")==0)
    {
        if( (sb.st_mode & S_IXOTH) && check_parent_dir_permissions(S_IXOTH)==0)
        {
            printf("(everyone)\n");
                    return 0;
        }
            /* code */
        if( (sb.st_mode & S_IXGRP) && check_parent_dir_permissions(S_IXGRP)==0 )
        {
            //  printf("Group can delete\n");

            printgroups(sb);
            printf("root\n");
            return 0;
        }
        if( (sb.st_mode & S_IXUSR) && check_parent_dir_permissions(S_IXUSR)==0 )
        {
            //printf("owner can delete\n");

            printowner(sb);
            printf("root\n");    
        }
        
        
        //return 0;

    }

    else if (strcmp(argv[1], "ls")==0)
    {
        if (S_ISDIR(sb.st_mode))
        {
            if( sb.st_mode & S_IROTH )
            {
                printf("(everyone)\n");
                return 0;
            }
            if( sb.st_mode & S_IRGRP )
            {
            //        printf("Group can ls\n");
                    
                printgroups(sb);
                printf("root\n");
            }
            if( sb.st_mode & S_IRUSR )
            {
            //    printf("owner can ls\n");
                
                printowner(sb);
                printf("root\n");
            }
        }

        else if (S_ISREG(sb.st_mode))
        {
            if( check_parent_dir_permissions(S_IROTH)==0)
            {
                printf("(everyone)\n");
                return 0;
            }
            if(check_parent_dir_permissions(S_IRGRP)==0)
            {
            //        printf("Group can ls\n");
                    
                printgroups(sb);
                printf("root\n");
            }
            if(check_parent_dir_permissions(S_IRUSR)==0)
            {
            //    printf("owner can ls\n");
                
                printowner(sb);
                printf("root\n");
            }
        }
    }

    else if (strcmp(argv[1], "read")==0)
    {
        if( sb.st_mode & S_IROTH && check_parent_dir_permissions(S_IXOTH)==0)
        {
            printf("(everyone)\n");
            return 0;
        }
        if( sb.st_mode & S_IRGRP && check_parent_dir_permissions(S_IXGRP)==0)
            {
                //printf("Group can read\n");
                
                printgroups(sb);
                
            }
        if( sb.st_mode & S_IRUSR )
        {
            //printf("owner can read\n");
            
                printowner(sb);
                
            }
        printf("root\n");
    }

    else if (strcmp(argv[1], "search")==0)
    {
        if( sb.st_mode & S_IRUSR )
        {
            //printf("owner can search\n");
            if( sb.st_mode & S_IRGRP && check_parent_dir_permissions(S_IXGRP)==0)
            {
              //  printf("Group can search\n");
                if( sb.st_mode & S_IROTH && check_parent_dir_permissions(S_IXOTH)==0)
                {
                        printf("(everyone)\n");
                        return 0;
                    }
                    printgroups(sb);
                }
                printowner(sb);
            }
        printf("root\n");
    }

    else if (strcmp(argv[1], "write")==0)
    {
        if( sb.st_mode & S_IWUSR )
        {
            //printf("owner can write\n");

            if( sb.st_mode & S_IWGRP && !(sb.st_mode & S_ISVTX) && check_parent_dir_permissions(S_IXGRP)==0)
            {
              //  printf("Group can write\n");
                if( sb.st_mode & S_IWOTH && check_parent_dir_permissions(S_IXOTH)==0)
                {
                        printf("(everyone)\n");
                        return 0;
                    }
                    printgroups(sb);
                }
                printowner(sb);
            }
        printf("root\n");
    }
    else
    {
        printf ("no action found");
    }

return 0;
    // printf("Mode:                     %lo (octal)\n", (unsigned long) sb.st_mode);

    // printf("Ownership:                UID=%ld   GID=%ld\n", (long) sb.st_uid, (long) sb.st_gid);

//     struct passwd *pwent;
//     long mxnGids = sysconf(_SC_NGROUPS_MAX);
//     int nGids;
//     gid_t *gids;
//     struct group *group;


//     gids = (gid_t *) malloc(mxnGids * sizeof(gid_t));

//     while ((pwent = getpwent()) != NULL) {
//         if(sb.st_uid==pwent->pw_uid)
//         {
//             printf("%s\n",pwent->pw_name);
//         }
//         printf("gids: ");
//         nGids = mxnGids; // maximum possible # of gids (input)
//         nGids = getgrouplist(pwent->pw_name, pwent->pw_gid, gids, &nGids);
//         assert(nGids > 0);
//         for (int i = 0; i < nGids; i++) {
//             group = getgrgid(gids[i]);
//             if (group == NULL)
//                 printf("%d (no group name found)", gids[i]);
//             else if (i == 0)
//                 printf("%d (%s)", gids[i], group->gr_name);
//             else
//                 printf(", %d (%s)", gids[i], group->gr_name);
//         }
//         printf("\n");
//         printf("\n");
//     }
//     return 0;
 }

