#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>



int main(int argc, char** argv){

	int bufferSize = atoi(argv[1]);
	int inputFilename = open(argv[2], O_RDONLY);
	int outputFilename = open(argv[3], O_WRONLY|O_TRUNC|O_CREAT, 00700);
	char* bufferContainer = malloc(bufferSize*sizeof(char));
	ssize_t bytesRead;
	while ((bytesRead = read(inputFilename, bufferContainer, bufferSize))!=0)
	{
		write(outputFilename, bufferContainer, bytesRead);
	}
	free(bufferContainer);
	close(inputFilename);
	close(outputFilename);

	return 0;
}
