// It's not a bad practice to list *why* you include particular
// headers.
#include <stdio.h> // for FILE, NULL, fopen(), and getc()

#include "eprintf.h" // for eprintf_fail()

// Although not strictly required, its a good practice to include the
// header file that corresponds to this source file to detect any
// discrepancies between the function's declaration and its
// definition.
#include "compare_files.h"


int compareFiles(char *fname0, char *fname1)
{
    //
    // ASSIGNMENT
    //
    // This function compares two files named `fname0` and `fname1`
    // and returns true (1) if they are identical or false (0) if they
    // are not. Here's how it would be described in pseudocode (note
    // the indented block structure):
    //
    //   open file 0 for reading (hint: fopen(3))
    //   if the open fails,
    //       exit with an error message (hint: eprintf_fail())
    //   open file 1 for reading (hint: fopen(3))
    //   if the open fails,
    //       exit with an error message (hint: eprintf_fail())
    //   repeat until this function returns:
    //       read a character `ch0` from file 0 (hint: getc(3))
    //       read a character `ch1` from file 1 (hint: getc(3))
    //       compare both characters to each other and to `EOF`,
    //        (possibly) returning 0 or 1
    //
	// The last statement is intentionally vague. The logic here is
    // important. No extra points challenge: It can be done in a
    // single `if`-block.
    //
    	//const char fname0, fname1;
	FILE *file0 = fopen(fname0, "r");
	if (file0==NULL)
	{
		eprintf_fail("Open of file %s failed\n",fname0);
	};
	FILE *file1 = fopen(fname1, "r");
	
	if (file1==NULL)
	{
		eprintf_fail("Open of file %s failed\n",fname1);
	};

	if (((getc(file0)) == EOF && (getc(file1))!=EOF) || ((getc(file0))!=EOF && (getc(file1))==EOF))
	{
		return 0;
	}

	while ((getc(file0)) != EOF && (getc(file1)) != EOF)
	{
		if (fgetc(file0) != fgetc(file1))
			return 0;
	};

	return 1;
}
