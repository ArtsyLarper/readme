#include <stdio.h>
#include <assert.h> // for assert()
#include <ctype.h>

struct CodeStats {
    int lineCount;
    int linewCode;
    int Cppcomments;
    int InCComments;
};


void codeStats_init(struct CodeStats *codeStats)
{
    codeStats->lineCount = 0;
    codeStats->linewCode = 0;
    codeStats->Cppcomments = 0;
    codeStats->InCComments = 0;
}


void codeStats_print(struct CodeStats codeStats, char *fileName)
{
    printf("           file: %s\n", fileName);
    printf("     line count: %d\n", codeStats.lineCount);
    printf("lines with code: %d\n", codeStats.linewCode);
    printf("   C++ comments: %d\n", codeStats.Cppcomments);
    printf("Cstyle comments: %d\n", codeStats.InCComments);
}


void codeStats_accumulate(struct CodeStats *codeStats, char *fileName)
{
    FILE *f = fopen(fileName, "r");
    int ch;
    int foundCodeOnLine;
    enum {
        START, FOUNDSLASH, INCPPCOMM, INCCOMMENT, FOUNDSTARINC, INSTRING, FOUNDESCAPE
    } state = START;
    assert(f);
    while ((ch = getc(f)) != EOF) {
        switch (state) {

        case START:
            if (ch == '\n') {
                	codeStats->lineCount++;
			codeStats->linewCode += foundCodeOnLine;
			foundCodeOnLine = 0;
            }
	    else if (isspace(ch))
	    {

	    }
	    else if (ch == '/' )
	    {
		state = FOUNDSLASH;
	    }
	    else if (ch == '\"')
	    {
		    state = INSTRING;
	    }
	    else
	    {
		    foundCodeOnLine = 1;
	    }
            break;

	case FOUNDSLASH:
		if (ch == '/')
		{	
			state = INCPPCOMM;
		}
		if (ch == '\n') {
                        codeStats->lineCount++;
                        codeStats->linewCode += foundCodeOnLine;
                        foundCodeOnLine = 0;
			state = START;
            	}
		if (ch == '*')
		{
			codeStats->InCComments ++;
			state = INCCOMMENT;
		}

		break;

	case INCPPCOMM:
		
		codeStats->Cppcomments ++;
	
		if (ch == '\n')
		{
			foundCodeOnLine = 1;
			codeStats->lineCount++;
                       	codeStats->linewCode += foundCodeOnLine;
                       	foundCodeOnLine = 0;
			state = START;
		}
		else
		{
			state = INCPPCOMM;
		}
		break;

	case INCCOMMENT:

		if (ch == '\n') {
                        codeStats->lineCount++;
                        codeStats->linewCode += foundCodeOnLine;
                        foundCodeOnLine = 0;
			state = INCCOMMENT;
           	 }
		if (ch == '*'){
			state = FOUNDSTARINC;
		}
		else
		{
			state = INCCOMMENT;
		}
		break;
				
	case FOUNDSTARINC:
		if (ch == '\n') {
                        codeStats->lineCount++;
                        codeStats->linewCode += foundCodeOnLine;
                        foundCodeOnLine = 0;
            	}
		if (ch == '/'){
			state = START;
		}
		else{
		state = INCCOMMENT;
		}
		break;
	case INSTRING:
		if (ch == '"')
		{
			case = START;
		}
		else if (ch == '\\')
		{
			case = FOUNDESCAPE;
		}
		else
		{
			case = INSTRING;
		}
		break;
	case FOUNDESCAPE:
		if (ch == '\n') {
			codeStats->lineCount++;
                        codeStats->linewCode += foundCodeOnLine;
                        foundCodeOnLine = 1;
			case = INSTRING;
		else 
		{
			case = INSTRING;
		}
		break;


	default:
            assert(0);
            break;
        }
    }
    fclose(f);
    assert(state == START);
}


int main(int argc, char *argv[])
{
    struct CodeStats codeStats;
    int i;

    for (i = 1; i < argc; i++) {
        codeStats_init(&codeStats);
        codeStats_accumulate(&codeStats, argv[i]);
        codeStats_print(codeStats, argv[i]); // no "&" -- see why?
        if (i != argc-1)   // if this wasn't the last file ...
            printf("\n");  // ... print out a separating newline
    }
    return 0;
}
