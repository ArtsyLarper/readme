#include <stdio.h>
#include "rational.h"
#include <math.h>
#include <stdlib.h>

static Rational rtnl_simplify(Rational rtnl)
{
	int a, b, tmp;
	a = abs(rtnl.num);
	b = abs(rtnl.denom);
	while (b != 0)
	{
		tmp = b;
		b = a % b;
		a = tmp;
	}
	rtnl.num = rtnl.num/a;
	rtnl.denom = rtnl.denom/a;
	if (rtnl.denom < 0)
	{
		rtnl.num = rtnl.num * -1;
		rtnl.denom = rtnl.denom * -1;
	}
	return rtnl;
}

Rational rtnl_add(Rational rtnl0, Rational rtnl1)
{
	Rational rtnl2;
	rtnl2.denom = rtnl0.denom * rtnl1.denom;
	int num1, num2;
	num1 = rtnl0.num * rtnl1.denom;
	num2 = rtnl1.num * rtnl0.denom;
	rtnl2.num = num1 + num2;
	return rtnl_simplify(rtnl2);
};

Rational rtnl_sub(Rational rtnl0, Rational rtnl1)
{
	
        Rational rtnl2;
        rtnl2.denom = rtnl0.denom * rtnl1.denom;
        int num1, num2;
        num1 = rtnl0.num * rtnl1.denom;
        num2 = rtnl1.num * rtnl0.denom;
        rtnl2.num = num1 - num2;
	return rtnl_simplify(rtnl2);
};

Rational rtnl_mul(Rational rtnl0, Rational rtnl1)
{
        Rational rtnl2;
        rtnl2.denom = rtnl0.denom * rtnl1.denom;
        rtnl2.num = rtnl0.num * rtnl1.num;
	return rtnl_simplify(rtnl2);
};


Rational rtnl_div(Rational rtnl0, Rational rtnl1)
{
	
        Rational rtnl2;
        int num1, num2;
        num1 = rtnl0.num * rtnl1.denom;
        num2 = rtnl1.num * rtnl0.denom;
        rtnl2.num = num1;
	rtnl2.denom = num2;
	return rtnl_simplify(rtnl2);
};

Rational rtnl_init(int num, int denom)
{
	Rational rtnl0;
	rtnl0.num = num;
	rtnl0.denom = denom;
	return rtnl_simplify(rtnl0);
};

Rational rtnl_ipow(Rational rtnl, int ipow)
{
	if (ipow == 0)
	{
		rtnl.num = 1;
		rtnl.denom = 1;
	}
	if (ipow > 0)
	{
		rtnl.num = pow(rtnl.num, ipow);
		rtnl.denom = pow(rtnl.denom, ipow);
	}
	if (ipow < 0)
	{
		rtnl.num = pow(rtnl.denom, abs(ipow));
		rtnl.denom = pow(rtnl.num, abs(ipow));
	}
	return rtnl_simplify(rtnl);
};

char *rtnl_asStr(Rational rtnl, char buf[RTNL_BUF_SIZE])
{
	sprintf(buf, "%d/%d", rtnl.num, rtnl.denom);
	return buf;
};

